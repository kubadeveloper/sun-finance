<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Services\PrintNumberFactory;
use App\PrintNumbers;

$factory = new PrintNumberFactory();
$numbers = new PrintNumbers(100, $factory);
$numbers->print();
