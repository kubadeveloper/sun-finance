<?php

namespace App\Services;

use App\Contracts\IPrint;

class PrintSomething implements IPrint
{
    /**
     * @var mixed Print contents.
     */
    protected $content;

    /**
     * PrintSomething constructor.
     *
     * @param mixed $content
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Print.
     *
     * @return void
     */
    public function print() : void
    {
        echo $this->content;
    }
}
