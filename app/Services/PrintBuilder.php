<?php

namespace App\Services;

use App\Contracts\IPrint;

class PrintBuilder
{
    /**
     * @var array Words.
     */
    protected $words = [];

    /**
     * Add some word.
     *
     * @param string $word
     * @return void
     */
    public function addWord(string $word) : void
    {
        $this->words[] = $word;
    }

    /**
     * Get words.
     *
     * @return string
     */
    public function getWords() : string
    {
        return implode(', ', $this->words);
    }

    /**
     * Get PrintSomething instance.
     *
     * @return IPrint
     */
    public function build() : IPrint
    {
        return new PrintSomething($this->getWords());
    }
}
