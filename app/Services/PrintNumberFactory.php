<?php

namespace App\Services;

use App\Contracts\IPrint;
use App\Contracts\IPrintNumberFactory;

class PrintNumberFactory implements IPrintNumberFactory
{
    /**
     * Get PrintSomething instance.
     *
     * @param int $number
     * @return IPrint
     */
    public function make(int $number) : IPrint
    {
        // It if the number is a multiple of all.
        if ($number % 3 === 0 || $number % 5 === 0 || $number % 15 === 0) {
            $builder = new PrintBuilder();

            if ($number % 3 === 0) $builder->addWord('Sun');
            if ($number % 5 === 0) $builder->addWord('Finance');
            if ($number % 15 === 0) $builder->addWord('SunFinance');

            return $builder->build();
        } else {
            return new PrintSomething($number);
        }
    }
}
