<?php

namespace App;

use App\Contracts\IPrintNumberFactory;

class PrintNumbers
{
    /**
     * @var int Count of numbers to print.
     */
    protected $count;

    /**
     * @var IPrintNumberFactory Print number factory.
     */
    protected $factory;

    /**
     * PrintNumbers constructor.
     *
     * @param int $count
     * @param IPrintNumberFactory $factory
     * @return void
     */
    public function __construct(int $count, IPrintNumberFactory $factory)
    {
        $this->count   = $count;
        $this->factory = $factory;
    }

    /**
     * Print numbers.
     *
     * @return void
     */
    public function print() : void
    {
        for ($i = 1; $i <= $this->count; $i++) {
            $this->factory->make($i)->print();
            echo nl2br(PHP_EOL);
        }
    }
}
