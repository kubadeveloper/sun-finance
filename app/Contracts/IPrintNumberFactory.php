<?php

namespace App\Contracts;

interface IPrintNumberFactory
{
    /**
     * Get IPrint instance.
     *
     * @return IPrint
     */
    public function make(int $number) : IPrint;
}
