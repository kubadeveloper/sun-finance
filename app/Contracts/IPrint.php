<?php

namespace App\Contracts;

interface IPrint
{
    /**
     * Print something.
     *
     * @return void
     */
    public function print() : void;
}
