#### Project deployment instructions

* **Clone the project from the repository.**
```bash
git clone https://kubadeveloper@bitbucket.org/kubadeveloper/sun-finance.git
```

* **Run this command for autoload namespaces.**
```bash
composer dump-autoload
```
